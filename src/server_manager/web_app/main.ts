// Tiptoe - Browse like nobody's watching
// Copyright (C) 2019  Josh Habdas <jhabdas@protonmail.com>
//
// This file is part of Tiptoe.
//
// Tiptoe is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tiptoe is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Tiptoe.  If not, see <https://www.gnu.org/licenses/>.
//
// This file incorporates work covered by the following copyright and
// permission notice:
//
//     Copyright 2018 The Outline Authors
//
//     Licensed under the Apache License, Version 2.0 (the "License");
//     you may not use this file except in compliance with the License.
//     You may obtain a copy of the License at
//
//          http://www.apache.org/licenses/LICENSE-2.0
//
//     Unless required by applicable law or agreed to in writing, software
//     distributed under the License is distributed on an "AS IS" BASIS,
//     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//     See the License for the specific language governing permissions and
//     limitations under the License.

import * as url from 'url';

import * as digitalocean_api from '../cloud/digitalocean_api';

import {App} from './app';
import {DigitalOceanTokenManager} from './digitalocean_oauth';
import * as digitalocean_server from './digitalocean_server';
import {DisplayServerRepository} from './display_server';
import {ManualServerRepository} from './manual_server';

function ensureString(queryParam: string|string[]): string {
  if (Array.isArray(queryParam)) {
    // We pick the last one if the parameter appears multiple times.
    return queryParam[queryParam.length - 1];
  } else {
    return queryParam;
  }
}

document.addEventListener('WebComponentsReady', () => {
  // Parse URL query params.
  const queryParams = url.parse(document.URL, true).query;
  const debugMode = ensureString(queryParams.outlineDebugMode) === 'true';
  const metricsUrl = ensureString(queryParams.metricsUrl);
  const shadowboxImage = ensureString(queryParams.image);
  const version = ensureString(queryParams.version);

  // Set DigitalOcean server repository parameters.
  const digitalOceanServerRepositoryFactory = (session: digitalocean_api.DigitalOceanSession) => {
    return new digitalocean_server.DigitaloceanServerRepository(
        session, shadowboxImage, metricsUrl, debugMode);
  };

  // Create and start the app.
  new App(
      document.getElementById('appRoot'), version,
      digitalocean_api.createDigitalOceanSession, digitalOceanServerRepositoryFactory,
      new ManualServerRepository('manualServers'), new DisplayServerRepository(),
      new DigitalOceanTokenManager())
      .start();
});
