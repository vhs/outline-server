// Tiptoe - Browse like nobody's watching
// Copyright (C) 2019  Josh Habdas <jhabdas@protonmail.com>
//
// This file is part of Tiptoe.
//
// Tiptoe is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tiptoe is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Tiptoe.  If not, see <https://www.gnu.org/licenses/>.
//
// This file incorporates work covered by the following copyright and
// permission notice:
//
//     Copyright 2018 The Outline Authors
//
//     Licensed under the Apache License, Version 2.0 (the "License");
//     you may not use this file except in compliance with the License.
//     You may obtain a copy of the License at
//
//          http://www.apache.org/licenses/LICENSE-2.0
//
//     Unless required by applicable law or agreed to in writing, software
//     distributed under the License is distributed on an "AS IS" BASIS,
//     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//     See the License for the specific language governing permissions and
//     limitations under the License.

import {ipcRenderer} from 'electron';
import {URL} from 'url';

import * as digitalocean_oauth from './digitalocean_oauth';

// This file is run in the renderer process *before* nodeIntegration is disabled.
//
// Use it for main/renderer process communication.

// DSN is all we need to specify; for all other config - breadcrumbs, etc., see the main process.
const params = new URL(document.URL).searchParams;

// tslint:disable-next-line:no-any
(window as any).whitelistCertificate = (fingerprint: string) => {
  return ipcRenderer.sendSync('whitelist-certificate', fingerprint);
};

// tslint:disable-next-line:no-any
(window as any).openImage = (basename: string) => {
  ipcRenderer.send('open-image', basename);
};

// tslint:disable-next-line:no-any
(window as any).onUpdateDownloaded = (callback: () => void) => {
  ipcRenderer.on('update-downloaded', callback);
};

// tslint:disable-next-line:no-any
(window as any).runDigitalOceanOauth = digitalocean_oauth.runOauth;

// tslint:disable-next-line:no-any
(window as any).bringToFront = () => {
  return ipcRenderer.send('bring-to-front');
};
