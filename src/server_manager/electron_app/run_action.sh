#!/bin/bash -eu
# Tiptoe - Browse like nobody's watching
# Copyright (C) 2019  Josh Habdas <jhabdas@protonmail.com>
#
# This file is part of Tiptoe.
# 
# Tiptoe is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Tiptoe is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with Tiptoe.  If not, see <https://www.gnu.org/licenses/>.
#
# This file incorporates work covered by the following copyright and
# permission notice:
# 
#     Copyright 2018 The Outline Authors
#
#     Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#          http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.

do_action server_manager/electron_app/build

readonly NODE_MODULES_BIN_DIR=$ROOT_DIR/src/server_manager/node_modules/.bin

cd $BUILD_DIR/server_manager/electron_app/static
OUTLINE_DEBUG=true \
SB_METRICS_URL=https://metrics-test.uproxy.org \
$NODE_MODULES_BIN_DIR/electron .
